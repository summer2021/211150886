# 1 下载安装

linux是一种操作系统内核，只要用了这个内核的操作系统都属于linux系统，ubuntu和debian都是用的linux内核

![image-20210807015529437](anaconda_help.assets/image-20210807015529437.png)

## 环境变量

Windows：

![image-20210807020012418](anaconda_help.assets/image-20210807020012418.png)

![image-20210807020150712](anaconda_help.assets/image-20210807020150712.png)

![image-20210807020219281](anaconda_help.assets/image-20210807020219281.png)

# 2 Vscode 配置

下载安装

添加插件

![image-20210807020434774](anaconda_help.assets/image-20210807020434774.png)

选择环境（选择编译器）：

![image-20210807020543046](anaconda_help.assets/image-20210807020543046.png)

![image-20210807020801539](anaconda_help.assets/image-20210807020801539.png)

![image-20210807020921146](anaconda_help.assets/image-20210807020921146.png)

# 3 环境

## 创建环境

conda create -n tf python=3.8

## 变更环境

conda activate tf

![image-20210807021758210](anaconda_help.assets/image-20210807021758210.png)

### 删除环境

**conda remove -n your_env_name -all**



## 安装第三方库：

第一步，去官网或github看一看，有没有安装指南：

典型例子就是pytorch

![image-20210807022558198](anaconda_help.assets/image-20210807022558198.png)



​	首先进入对应环境：conda activate tf

​	然后运行 conda install <库名>（备选）或者python -m pip install <库名>

# 4 md文件

[Typora — a markdown editor, markdown reader.](https://typora.io/)

图片设置

<img src="anaconda_help.assets/image-20210807023259455.png" alt="image-20210807023259455" style="zoom:50%;" />

<img src="anaconda_help.assets/image-20210807023338579.png" alt="image-20210807023338579" style="zoom:50%;" />

