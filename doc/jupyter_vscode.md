# 安装vscode

百度搜索vscode，下载对应版本



# 安装插件

![image-20210724104221540](jupyter_vscode.assets/image-20210724104221540.png)

![image-20210724103959032](jupyter_vscode.assets/image-20210724103959032.png)



安装以上两个插件

# 重启vscode

左下角状态栏里选择python编译器

![image-20210724104249702](jupyter_vscode.assets/image-20210724104249702.png)

![image-20210724104324164](jupyter_vscode.assets/image-20210724104324164.png)

之前安装过的话列表中会出现对应的选项。

安装python之后刷新列表：

![image-20210724104638293](jupyter_vscode.assets/image-20210724104638293.png)

选择编译器之后左下角状态栏出现当前python版本，这时选择对应jupyter脚本就可以显示出来啦：

![image-20210724104711352](jupyter_vscode.assets/image-20210724104711352.png)

![image-20210724104811267](jupyter_vscode.assets/image-20210724104811267.png)

# 安装pip

百度搜索pip，进入pip官网

![image-20210724105100421](jupyter_vscode.assets/image-20210724105100421.png)

下载pip压缩包

解压之后在解压过后的文件夹中找到setup.py，在setup.py所在文件夹下运行（运行之前确保已经安装python，并且已在环境变量中）：

```shell
python setup.py install
```



# 安装必要库：

```
pip install pandas
pip install numpy
pip install plotly
pip install jupyter

```

# 之后就可运行.ipynb文件

（需要注意的是，以上步骤中的pip安装过程，在安装anaconda的方案中可以忽略，换句话说，pip已经被anaconda工具包所包含）

![image-20210724112354488](jupyter_vscode.assets/image-20210724112354488.png)